SQAFFOLD directory structure
  ¦--data
  ¦   ¦--010---raw-sources
  ¦   ¦   ¦--*001_SourceExample_deleteMe.txt
  ¦   ¦--020---cleaned-sources
  ¦   ¦--030---sources-with-uids
  ¦   ¦--040---coded-sources
  ¦   ¦--041---coded-sources-for-merging
  ¦   ¦   ¦--001_Source
  ¦   ¦   ¦   ¦--*001_Source_cleaned_withUIDs_coder1.rock
  ¦   ¦--090---masked-sources
  ¦   ¦--*case-attributes_deleteMe.rock
  ¦   ¦--*readMe--Data.txt
  ¦--dissemination 
  ¦   ¦--manuscripts
  ¦   ¦   ¦--*SQAFFOLD--Manuscript_checklist.txt
  ¦   ¦   ¦--*readMe--Manuscripts.txt
  ¦   ¦--presentations
  ¦   ¦   ¦--*readMe--Presentations.txt
  ¦   ¦--resources
  ¦   ¦   ¦--*SQAFFOLD_logo.txt
  ¦   ¦--*readMe--Dissemination.txt
  ¦   ¦--*SQAFFOLD--List_of_project_outputs.txt
  ¦--operationalization 
  ¦   ¦--analysis-plans
  ¦   ¦   ¦--mock-ups
  ¦   ¦   ¦--*readMe--Analysis_plans.txt
  ¦   ¦--code-development
  ¦   ¦   ¦--deductive
  ¦   ¦   ¦--inductive
  ¦   ¦   ¦   ¦--CodeDev__Round_1
  ¦   ¦   ¦   ¦   ¦--Coder_1
  ¦   ¦   ¦   ¦   ¦--Coder_2
  ¦   ¦   ¦   ¦   ¦--*SQAFFOLD--Round_1_coding_instructions.txt
  ¦   ¦   ¦   ¦--Triangulation_Round_1
  ¦   ¦   ¦   ¦   ¦--*SQAFFOLD--Triangulation_Round1__Meeting_Notes.txt
  ¦   ¦   ¦--*readMe--Code_development.txt
  ¦   ¦--coder-agreement
  ¦   ¦   ¦--inter-coder-agreement
  ¦   ¦   ¦   ¦--*SQAFFOLD--Inter-coder_coding_instructions.txt
  ¦   ¦   ¦--intra-coder-agreement
  ¦   ¦   ¦   ¦--*SQAFFOLD--Intra-coder_coding_instructions.txt
  ¦   ¦   ¦--*readMe--Coder_agreement.txt
  ¦   ¦--coding
  ¦   ¦   ¦--coder-training
  ¦   ¦   ¦   ¦--*SQAFFOLD--Coder_training_coding_instructions.txt
  ¦   ¦   ¦   ¦--*SQAFFOLD--Coder_training_materials.txt
  ¦   ¦   ¦   ¦--*SQAFFOLD--Coder_training_notes.txt
  ¦   ¦   ¦--final-codebook
  ¦   ¦   ¦--*readMe--Coding.txt
  ¦   ¦   ¦--*SQAFFOLD--Final_coding_instructions.txt
  ¦   ¦--data-collection
  ¦   ¦   ¦--data-collection-instruments
  ¦   ¦   ¦--data-collection-training
  ¦   ¦   ¦   ¦--*SQAFFOLD--Data_collection_instructions.txt
  ¦   ¦   ¦--*readMe--Data_collection.txt
  ¦   ¦--data-management-plan
  ¦   ¦   ¦--*readMe--DMP.txt
  ¦   ¦--data-transformations
  ¦   ¦   ¦--data-anonymization
  ¦   ¦   ¦   ¦--*SQAFFOLD--Instructions_for_anonymization.txt
  ¦   ¦   ¦--data-transcription
  ¦   ¦   ¦   ¦--*SQAFFOLD--Instructions_for_transcription.txt
  ¦   ¦   ¦--*readMe--Data_transformations.txt
  ¦   ¦--recruitment
  ¦   ¦   ¦--information-and-consent-forms
  ¦   ¦   ¦--*readMe--Recruitment.txt
  ¦   ¦--segmentation-development
  ¦   ¦   ¦--*readMe--Segmentation.txt
  ¦   ¦   ¦--*SQAFFOLD--Segmentation_instructions.txt
  ¦   ¦--*readMe--ToDos.txt
  ¦   ¦--*SQAFFOLD--Operationalization.txt
  ¦   ¦--*SQAFFOLD--Conceptualization.txt
  ¦   ¦--*SQAFFOLD--Reflexivity_journal.txt
  ¦--preregistration
  ¦   ¦--*readMe--Preregistration.txt
  ¦--private
  ¦   ¦--budget
  ¦   ¦   ¦--readMe--Budget.txt
  ¦   ¦--literature
  ¦   ¦   ¦--*readMe--Literature.txt
  ¦   ¦--*readMe--Private
  ¦--results
  ¦   ¦--*readMe--Results.txt
  ¦--scripts
  ¦   ¦--*sqaffold.html
  ¦   ¦--*sqaffold.rmd
  ¦--*.gitignore
  ¦--*.gitlab-ci.yml
  ¦--*directory_structure.txt
  ¦--*README.md
  ¦--*README.txt
  ¦--*sqaffold.Rproj